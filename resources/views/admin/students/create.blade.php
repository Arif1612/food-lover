<x-admin.master>
    <x-slot:title>
        Student Create
        </x-slot>
        <main id="main" class="main">
            {{-- sir ar theke uporer part ta  --}}
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Students</h1>
                <div class="btn-toolbar mb-2 mb-md-0">

                    <a href="{{ route('students.index') }}">
                        <button type="button" class="btn btn-sm btn-outline-info">
                            <span data-feather="List"></span>
                            List
                        </button>
                    </a>
                </div>
            </div>

            {{-- validation Check message --}}

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{-- Form dite hobe --}}
            <form action="{{ route('students.store') }} " method="post">
                @csrf
                <div class="mb-3">
                    <label for="nameInput" class="form-label ">Name</label>
                    <input type="text" name="name" class="form-control    @error('name') is-invalid @enderror"
                        id="exampleInputEmail1" aria-describedby="emailHelp">

                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="mb-3 form-check">
                    <input name="is_active" type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="isActiveInput">Is Active ?</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </main>




</x-admin.master>
