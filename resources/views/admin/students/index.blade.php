<x-admin.master>
    <x-slot:title>
        Student Details
    </x-slot:title>
    <main id="main" class="main">
        @if (session('message'))
            <span class="text-success">{{ session('message') }} </span>
        @endif

        {{-- sir ar theke uporer part ta  --}}
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Students</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group me-2">
                    {{-- <a href="{{ route('categories.pdf') }}"> --}}
                    <button type="button" class="btn btn-sm btn-outline-secondary">PDF</button>
                    </a>
                    <button type="button" class="btn btn-sm btn-outline-secondary">Excel</button>
                    {{-- <a href="{{ route('categories.trash') }}"> --}}
                    <button type="button" class="btn btn-sm btn-outline-danger">Trash</button>
                    </a>
                </div>
                <a href="{{ route('categories.create') }}">
                    <button type="button" class="btn btn-sm btn-outline-primary">
                        <span data-feather="plus"></span>
                        Add New
                    </button>
                </a>
            </div>
        </div>


        <table class="table">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Student Name </th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($students as $student)
                    <tr>
                        <td>{{ $loop->iteration }} </td>
                        <td>{{ $student->name }} </td>
                        <td>
                            <a class="btn btn-success" href="{{ route('students.show', $student->id) }} ">Show
                            </a> |

                            <a class="btn btn-warning" href="{{ route('students.edit', $student->id) }} ">Edit</a> |

                            <form action="{{ route('students.destroy', $student->id) }}" method="post"
                                style="display:inline">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" href="">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </main>
</x-admin.master>
