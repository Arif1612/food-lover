
<table class="table">
    <thead>
        <tr>
            <th>SL#</th>
            <th>Category Name </th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($categories as $category)
            <tr>
                <td>{{ $loop->iteration }} </td>
                <td>{{ $category->name }} </td>
            </tr>
        @endforeach

    </tbody>
</table>
<
