<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\ColorController;
use App\Models\Student;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';


Route::get('/', function () {
    return view('welcome');
});

// authentication
// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');


Route::get('/dashboard',[AdminController::class,'dashboard'])->name('dashboard');

Route::get('/categories/pdf', [CategoryController::class, 'downloadPdf'])->name('categories.pdf');

Route::get('/products/pdf', [CategoryController::class, 'downloadPdf'])->name('products.pdf');



// Resoursce Route
Route::resource('categories', CategoryController::class);
Route::resource('products', ProductController::class);

Route::get('/categories-trash', [CategoryController::class, 'trash'])->name('categories.trash');
Route::get('/categories/{id}/restore', [CategoryController::class, 'restore'])->name('categories.restore');
Route::delete('/categories/{id}/delete', [CategoryController::class, 'delete'])->name('categories.delete');

Route::get('/products-trash', [CategoryController::class, 'trash'])->name('products.trash');
Route::get('/products/{id}/restore', [CategoryController::class, 'restore'])->name('products.restore');
Route::delete('/products/{id}/delete', [CategoryController::class, 'delete'])->name('products.delete');

//              ******** color route **********



Route::get('/colors-trash', [ColorController::class, 'trash'])->name('colors.trash');
Route::get('/colors/{id}/restore', [ColorController::class, 'restore'])->name('colors.restore');
Route::delete('/colors/{id}/delete', [ColorController::class, 'delete'])->name('colors.delete');
Route::get('/colors/pdf', [ColorController::class, 'downloadPdf'])->name('colors.pdf');
Route::resource('colors', ColorController::class);


//                  ******categories all route start *********

// Route::get('/categories',[CategoryController::class,'index'])->name('categories.index');
// Route::get('/categories/create',[CategoryController::class,'create'])->name('categories.create');
// Route::post('/categories',[CategoryController::class,'store'])->name('categories.store');
// Route::get('/categories/{id}',[CategoryController::class,'show'])->name('categories.show') ;
// Route::delete('/categories/{id}',[CategoryController::class,'destroy'])->name('categories.destroy');
// Route::get('/categories/{id}/edit',[CategoryController::class,'edit'])->name('categories.edit');
// Route::patch('/categories/{id}',[CategoryController::class,'update'])->name('categories.update');




//                  ************ Student Route Start **********
Route::get('/students/create',[StudentController::class,'create'])->name('students.create');
Route::post('/students',[StudentController::class,'store'])->name('students.store');
Route::get('/students/{id}/edit',[StudentController::class,'edit'])->name('students.edit');
Route::patch('/students/{id}',[StudentController::class,'update'])->name('students.update');

// agei krse
Route::get('/students',[StudentController::class,'index'])->name('students.index');
Route::get('/students/{id}',[StudentController::class,'show'])->name('students.show');
Route::delete('/students/{id}',[StudentController::class,'destroy'])->name('students.destroy');



Route::fallback(function () {
    echo('route thik moto den mia');
});

