<?php

namespace Database\Factories;
use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Student>
 */
class StudentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'student_id'=>fake()->numberBetween(),
            'name'=>fake()-> sentence(5),
            'date_of_berth'=>fake()->date(),
            'gender'=>fake()->sentence(1),
            'hobbis' => fake()->sentence(30)
        ];
    }
}
