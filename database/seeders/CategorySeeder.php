<?php

namespace Database\Seeders;

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // model ar maddome korer example age model create kore nite hobe

        Category::Create([
            'name' => 'Fruits',
            'is_active' => true
        ]);
        Category::Create([
            'name' => 'Vegetables',
            'is_active' => true
        ]);
        Category::Create([
            'name' => 'Drinks',
            'is_active' => true
        ]);


         // query builder dia korer example
         DB::table('categories')->insert([
            'name' => 'Ingradiants',
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
