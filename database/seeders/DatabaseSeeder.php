<?php
namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            CategorySeeder::class,
            ProductSeeder::class,
            StudentSeeder::class
            // PostSeeder::class,
            // CommentSeeder::class,
        ]);
        // query builder dia korer example
        // DB::table('categories')->insert([
        //     'name' => 'Fruits',
        //     'is_active' => true,
        //     'created_at' => C arbon::now(),
        //     'updated_at' => Carbon::now()
        // ]);
        // model ar maddome korer example age model create kore nite hobe
        // Category::Create([
        //     'name' => 'Vegetables',
        //     'is_active' => true
        // ]);
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
