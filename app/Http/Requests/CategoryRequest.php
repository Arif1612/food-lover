<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $imgRule= 'required|mimes:png,jpg,jpeg,webp';
        if($this->isMethod('patch')){
            $imgRule="mimes:png,jpg,jpeg,webp";
            }

        $rules = [
            'name' => 'required|min:2|max:30 | unique:categories,name,'.$this->category?->id,
            'image' => $imgRule
        ];
        // dd($this->id);
        return $rules;
    }
}
