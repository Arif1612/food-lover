<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentRequest;
use App\Models\Student;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::all();
        return view('admin.students.index',compact('students'));
    }
    public function show($id)
    {
        $student = Student::find($id);
        return view('admin.students.show',compact('student'));
    }
    public function destroy($id)
    {
        $student = Student::find($id);
        $student->delete();
        return redirect()
        ->route('students.index')
        ->withMessage('Successfully Deleted');
    }
    public function create()
    {
        return view('admin.students.create');
    }
    public function store(StudentRequest $request)
    {
        $requestData = [
            'name'=>$request->name,
            'is_active'=>$request->is_active ? true:false
        ];

        Student::Create($requestData);
        return redirect()
        ->route('students.index')
        ->withMessage('Successfully Created');
    }


}
