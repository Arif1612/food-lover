<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Image;
use Barryvdh\DomPDF\Facade\Pdf;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.index',compact('categories'));
    }
    // route  model binding
    public function show(Category $category)
    {
        // route model binding ar maddome man ta nia asce tai r database theke alada kre kisu ante hobe hh
        // dd($id);
        // $category = Category::find($id);
        // dd($category);
        return view('admin.categories.show',compact('category'));
    }
    public function destroy($id)
    {
        // dd($category);
        $category = Category::find($id);
        // dd($category);
        $category->delete();
        return redirect()
        ->route('categories.index')
        ->withMessage('Successfully Deleted');
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    // form theke j input gula nisse oigual request ar maddome store kora abong create ar maddome database a pathanor kaj korse amra store method a
    public function store(CategoryRequest $request)
    {
        // img file ta ane storage a file path ta move kore dilam
        // $img =$request->file('image');
        // $imgFileName = date('Y-m-d').time().$img->getClientOriginalName();
        // $img->move(storage_path('app/public/categories'),$imgFileName);
// form theke request ar maddome input value gula nia asse
// dd($request);
        $requestData = [
            'name'=>$request->name,
            'is_active'=>$request->is_active ? true:false,
            'image' => $this->uploadImage($request->file('image'))
            // 'image'=>$imgFileName
        ];
        // dd($requestData);
        Category::create($requestData);
        return redirect()
        ->route('categories.index')
        ->withMessage('Successfully Created');
    }

    public function edit(Category $category)
    {
        // amra aikhane route model binding ar maddome database theke data ta nia asse route model binding krle route a j name dise thik Model ar por a sei name ee dite hobe thahole r data find kore ante hobe nhh

        // $category = Category::find($id);

        return view('admin.categories.edit',compact('category'));
    }
    public function update(CategoryRequest $request,Category $category)
    {
        // dd($id);
        // $category = Category::find($id);
        $requestData = [
            'name'=>$request->name,
            'is_active'=>$request->is_active ? true:false,
            // 'image'=>$imgFileName
        ];
        // dd($requestData);
        if ($request->hasFile('image')) {
            $requestData['image'] = $this->uploadImage($request->file('image'));
        }

        $category->update($requestData);
        // Category::Create($requestData);
        return redirect()
        ->route('categories.index')
        ->withMessage('Successfully Updated');
    }

    public function trash()
    {
        $categories = Category::onlyTrashed ()->get();
        return view('admin.categories.trash',compact('categories'));
    }
    public function delete($id)
    {
        $category = Category::onlyTrashed()->find($id);
        $category->forceDelete();
        return redirect()
        ->route('categories.trash')
        ->withMessage('Successfully Deleted');
        // return view('admin.categories.trash',compact('categories'));
    }
    public function restore($id)
    {
        $category = Category::onlyTrashed()->find($id);
        $category->restore();
        return redirect()
        ->route('categories.trash')
        ->withMessage('Successfully Restore');
        // return view('admin.categories.trash',compact('categories'));
    }

    public function uploadImage($image)
    {
        $originalName = $image->getClientOriginalName();
        $fileName = date('Y-m-d') . time() . $originalName;

        // $image->move(storage_path('/app/public/categories'), $fileName);

        Image::make($image)
            ->resize(200, 200)
            ->save(storage_path() . '/app/public/categories/' . $fileName);

        return $fileName;
    }

    public function downloadPdf()
    {
        $categories = Category::all();
        $pdf = Pdf::loadView('admin.categories.pdf', compact('categories'));
        return $pdf->download('category-list.pdf');
    }

}


